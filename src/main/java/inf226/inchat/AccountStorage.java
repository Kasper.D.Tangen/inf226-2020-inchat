package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;

import inf226.util.immutable.List;
import inf226.util.*;
import org.eclipse.jetty.util.security.Password;

/**
 * This class stores accounts in the database.
 */
public final class AccountStorage
    implements Storage<Account,SQLException> {
    
    final Connection connection;
    final Storage<User,SQLException> userStore;
    final Storage<Channel,SQLException> channelStore;
   
    /**
     * Create a new account storage.
     *
     * @param  connection   The connection to the SQL database.
     * @param  userStore    The storage for User data.
     * @param  channelStore The storage for channels.
     */
    public AccountStorage(Connection connection,
                          Storage<User,SQLException> userStore,
                          Storage<Channel,SQLException> channelStore) 
      throws SQLException {
        this.connection = connection;
        this.userStore = userStore;
        this.channelStore = channelStore;
        
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Account (id TEXT PRIMARY KEY, version TEXT, user TEXT, password TEXT ,FOREIGN KEY(user) REFERENCES User(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS AccountChannel (account TEXT, channel TEXT, alias TEXT, ordinal INTEGER, PRIMARY KEY(account,channel), FOREIGN KEY(account) REFERENCES Account(id) ON DELETE CASCADE, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Account> save(Account account)
      throws SQLException {
        
        final Stored<Account> stored = new Stored<Account>(account);
        final String sql =  "INSERT INTO Account VALUES(?, ?, ?, ?)";
        PreparedStatement prepStmt = connection.prepareStatement(sql);
        prepStmt.setObject(1, stored.identity);
        prepStmt.setObject(2, stored.version);
        prepStmt.setObject(3, account.user.identity);
        prepStmt.setString(4, account.hashedPassword.toString());
        prepStmt.executeUpdate();
        
        // Write the list of channels
        final Maybe.Builder<SQLException> exception = Maybe.builder();
        final Mutable<Integer> ordinal = new Mutable<Integer>(0);
        account.channels.forEach(element -> {
            String alias = element.first;
            Stored<Channel> channel = element.second;
            final String msql
              = "INSERT INTO AccountChannel VALUES(?, ?, ?, ?)";

            try {
                PreparedStatement prepStmt2 = connection.prepareStatement(msql);
                prepStmt2.setObject(1, stored.identity);
                prepStmt2.setObject(2, channel.identity);
                prepStmt2.setObject(3, alias);
                prepStmt2.setString(4, ordinal.get().toString());
                prepStmt2.executeUpdate();
            }
            catch (SQLException e) { exception.accept(e) ; }
            ordinal.accept(ordinal.get() + 1);
        });

        Util.throwMaybe(exception.getMaybe());
        return stored;
    }
    
    @Override
    public synchronized Stored<Account> update(Stored<Account> account,
                                            Account new_account)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Account> current = get(account.identity);
    final Stored<Account> updated = current.newVersion(new_account);
    if(current.version.equals(account.version)) {
        String sql = "UPDATE Account SET" +
            " (version,user) =(?, ?) WHERE id= ?";
        connection.createStatement().executeUpdate(sql);

        PreparedStatement prepStmt = connection.prepareStatement(sql);
        prepStmt.setObject(1, updated.version);
        prepStmt.setObject(2, new_account.user.identity);
        prepStmt.setObject(3, updated.identity);
        prepStmt.executeUpdate();

        
        // Rewrite the list of channels
        String sql2 = "DELETE FROM AccountChannel WHERE account=?";
        PreparedStatement prepStmt2 = connection.prepareStatement(sql2);
        prepStmt2.setObject(1, account.identity);
        prepStmt2.executeUpdate();

        final Maybe.Builder<SQLException> exception = Maybe.builder();
        final Mutable<Integer> ordinal = new Mutable<Integer>(0);
        new_account.channels.forEach(element -> {
            String alias = element.first;
            Stored<Channel> channel = element.second;
            final String msql = "INSERT INTO AccountChannel VALUES(?, ?, ?, ?)";
            try (PreparedStatement ps3 = connection.prepareStatement(msql)) {
                ps3.setObject(1, account.identity);
                ps3.setObject(2, channel.identity);
                ps3.setString(3, alias);
                ps3.setString(4, ordinal.get().toString());
                ps3.executeUpdate();
            }
            catch (SQLException e) { exception.accept(e) ; }
            ordinal.accept(ordinal.get() + 1);
        });

        Util.throwMaybe(exception.getMaybe());
    } else {
        throw new UpdatedException(current);
    }
    return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Account> account)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Account> current = get(account.identity);
        if(current.version.equals(account.version)) {
            String sql =  "DELETE FROM Account WHERE id=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setObject(1, account.identity);
            ps.executeUpdate();
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Account> get(UUID id)
      throws DeletedException,
             SQLException {

        final String accountsql = "SELECT version,user,password FROM Account WHERE id = '" + id.toString() + "'";
        final String channelsql = "SELECT channel,alias,ordinal FROM AccountChannel WHERE account = '" + id.toString() + "' ORDER BY ordinal DESC";

        final Statement accountStatement = connection.createStatement();
        final Statement channelStatement = connection.createStatement();

        final ResultSet accountResult = accountStatement.executeQuery(accountsql);
        final ResultSet channelResult = channelStatement.executeQuery(channelsql);

        if(accountResult.next()) {
            final UUID version = UUID.fromString(accountResult.getString("version"));
            final UUID userid =
            UUID.fromString(accountResult.getString("user"));
            final Stored<User> user = userStore.get(userid);
            // Get all the channels associated with this account
            final List.Builder<Pair<String,Stored<Channel>>> channels = List.builder();
            final String password = accountResult.getString("password");
            while(channelResult.next()) {
                final UUID channelId = 
                    UUID.fromString(channelResult.getString("channel"));
                final String alias = channelResult.getString("alias");
                channels.accept(
                    new Pair<String,Stored<Channel>>(
                        alias,channelStore.get(channelId)));
            }
            return (new Stored<Account>(new Account(user,channels.getList(), new Password(password)),id,version));
        } else {
            throw new DeletedException();
        }
    }
    
    /**
     * Look up an account based on their username.
     */
    public Stored<Account> lookup(String username)
      throws DeletedException,
             SQLException {

        final String sql = "SELECT Account.id from Account INNER JOIN User ON user=User.id where User.name=?";
        PreparedStatement prepStmt = connection.prepareStatement(sql);
        prepStmt.setObject(1, username);

        //System.err.println(sql);
        final ResultSet rs = prepStmt.executeQuery();
        if(rs.next()) {
            final UUID identity = 
                    UUID.fromString(rs.getString("id"));
            return get(identity);
        }
        throw new DeletedException();
    }

    /**
     * Gets the password based on the username
     */
    public String getPassword(String username) throws SQLException {
        final String sql = "SELECT Account.password FROM Account INNER JOIN User on user=User.id WHERE User.name=?";
        PreparedStatement prepStmt = connection.prepareStatement(sql);
        prepStmt.setString(1, username);

        final ResultSet result = prepStmt.executeQuery();
        return result.getString("password");
    }
    
} 
 
