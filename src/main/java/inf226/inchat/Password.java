package inf226.inchat;

public final class Password {

    public final String password;

    public Password(String password) {
        this.password = password;
    }


    public static boolean validPassword(String password) {
        int hPLength = password.length();
        if (hPLength >= 8 && hPLength <= 64) {
            int digits, upperCases, lowerCases;
            digits = upperCases = lowerCases = 0;
            for (char c : password.toCharArray()) {
                if (Character.isDigit(c)) {
                    digits++;
                }
                if (Character.isUpperCase(c)) {
                    upperCases++;
                }
                if (Character.isLowerCase(c)) {
                    lowerCases++;
                }
            }
            if (digits != 0 && upperCases != 0 && lowerCases != 0) {
                return true;
            }
            System.out.println("Password is invalid");
        }
        return false;
    }
}
