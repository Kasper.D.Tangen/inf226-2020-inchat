package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Consumer;

import inf226.storage.*;
import inf226.util.*;




public final class EventStorage
    implements Storage<Channel.Event,SQLException> {
    
    private final Connection connection;
    
    public EventStorage(Connection connection) 
      throws SQLException {
        this.connection = connection;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Event (id TEXT PRIMARY KEY, version TEXT, type INTEGER, time TEXT)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Message (id TEXT PRIMARY KEY, sender TEXT, content Text, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Joined (id TEXT PRIMARY KEY, sender TEXT, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Channel.Event> save(Channel.Event event)
      throws SQLException {
        
        final Stored<Channel.Event> stored = new Stored<Channel.Event>(event);
        
        String sql =  "INSERT INTO Event VALUES(?, ?, ?, ?)";
        PreparedStatement prepStmt = connection.prepareStatement(sql);
        prepStmt.setObject(1, stored.identity);
        prepStmt.setObject(2, stored.version);
        prepStmt.setObject(3, event.type.code);
        prepStmt.setObject(4, event.time);
        prepStmt.execute();

        switch (event.type) {
            case message:
                sql = "INSERT INTO Message VALUES(?, ?, ?)";
                PreparedStatement prepStmt2 = connection.prepareStatement(sql);
                prepStmt2.setObject(1, stored.identity);
                prepStmt2.setObject(2, event.sender);
                prepStmt2.setObject(3, event.message);
                prepStmt2.execute();
                break;
            case join:
                sql = "INSERT INTO Joined VALUES(?, ?)";
                PreparedStatement prepStmt3 = connection.prepareStatement(sql);
                prepStmt3.setObject(1, stored.identity);
                prepStmt3.setObject(2, event.sender);
                prepStmt3.execute();
                break;
        }
        return stored;
    }
    
    @Override
    public synchronized Stored<Channel.Event> update(Stored<Channel.Event> event,
                                            Channel.Event new_event)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Channel.Event> current = get(event.identity);
    final Stored<Channel.Event> updated = current.newVersion(new_event);
    if(current.version.equals(event.version)) {
        String sql = "UPDATE Event SET" +
            " (version,time,type) = (?, ?, ?) WHERE id=?";
        PreparedStatement prepStmt = connection.prepareStatement(sql);
        prepStmt.setObject(1, updated.version);
        prepStmt.setObject(2, new_event.time);
        prepStmt.setObject(3, new_event.type.code);
        prepStmt.setObject(4, updated.identity);
        prepStmt.execute();
        switch (new_event.type) {
            case message:
                sql = "UPDATE Message SET (sender,content)= (?, ?) WHERE id=?";
                PreparedStatement prepStmt2 = connection.prepareStatement(sql);
                prepStmt2.setObject(1, new_event.sender);
                prepStmt2.setObject(2, new_event.message);
                prepStmt2.setObject(3, updated.identity);
                prepStmt2.execute();
                break;
            case join:
                sql = "UPDATE Joined SET (sender)= (?) WHERE id=?";
                PreparedStatement prepStmt3 = connection.prepareStatement(sql);
                prepStmt3.setObject(1, new_event.sender);
                prepStmt3.setObject(2, updated.identity);
                prepStmt3.execute();
                break;
        }
    } else {
        throw new UpdatedException(current);
    }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Channel.Event> event)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Channel.Event> current = get(event.identity);
        if(current.version.equals(event.version)) {
        String sql =  "DELETE FROM Event WHERE id =?";
        PreparedStatement prepStmt = connection.prepareStatement(sql);
        prepStmt.setObject(1, event.identity);
        prepStmt.execute();
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Channel.Event> get(UUID id)
      throws DeletedException,
             SQLException {
        final String sql = "SELECT version,time,type FROM Event WHERE id = ?";
        final PreparedStatement prepStmt = connection.prepareStatement(sql);
        prepStmt.setObject(1,id.toString());
        final ResultSet rs = prepStmt.executeQuery();

        if(rs.next()) {
            final UUID version = UUID.fromString(rs.getString("version"));
            final Channel.Event.Type type = 
                Channel.Event.Type.fromInteger(rs.getInt("type"));
            final Instant time = 
                Instant.parse(rs.getString("time"));
            
            //final Statement mstatement = connection.createStatement();
            switch(type) {
                case message:
                    final String sql2 = "SELECT sender,content FROM Message WHERE id = ?";
                    PreparedStatement prepStmt2 = connection.prepareStatement(sql2);
                    prepStmt2.setObject(1, id.toString());
                    final ResultSet rs2 = prepStmt2.executeQuery();
                    rs2.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createMessageEvent(time,rs2.getString("sender"),rs2.getString("content")),
                            id,
                            version);
                case join:
                    final String sql3 = "SELECT sender FROM Joined WHERE id = ?";
                    PreparedStatement prepStmt3 = connection.prepareStatement(sql3);
                    prepStmt3.setObject(1, id.toString());
                    final ResultSet rs3 = prepStmt3.executeQuery();
                    rs3.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createJoinEvent(time,rs3.getString("sender")),
                            id,
                            version);
            }
        }
        throw new DeletedException();
    }
    
}


 
